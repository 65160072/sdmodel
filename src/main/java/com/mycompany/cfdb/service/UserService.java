/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.cfdb.service;

import com.mycompany.cfdb.dao.UserDao;
import com.mycompany.cfdb.model.User;

/**
 *
 * @author sTCFILMs
 */
public class UserService {
    public User login(String name,String password){
        UserDao userdao = new UserDao();
        User user = userdao.getByName(name); 
        if(user!=null && user.getPassword().equals(password)){
            return user; 
        }
        return null; 
    }
}
