/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.cfdb;

import com.mycompany.cfdb.dao.UserDao;
import com.mycompany.cfdb.helper.DBHelper;
import com.mycompany.cfdb.model.User;

/**
 *
 * @author sTCFILMs
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userdao = new UserDao();
        for (User u : userdao.getAll()) {
            System.out.println(u);
        }
        //User user1 = userdao.get(2);
        //System.out.println(user1);
      
       // User newUser = new User("user4","password",1,"M");
        //User insertedUser = userdao.save(newUser);
        //System.out.println(insertedUser);
        //insertedUser.setGender("M");
        //user1.setGender("F");
        //userdao.update(user1);
        //User updateUser = userdao.get(user1.getId());
        //System.out.println(updateUser);
        //userdao.delete(user1);
        //for (User u : userdao.getAll()) {
        //    System.out.println(u);
        //}
        for(User u :userdao.getAllOrderBy("user_name","asc")){
            System.out.println(u);
        }
        DBHelper.close();

    }
}
